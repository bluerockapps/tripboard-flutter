
import 'dart:async';

class CompanyBloc {
  final whitelistStreamController = StreamController.broadcast();

  Stream get getStream => whitelistStreamController.stream;

  final Map allUsers = {
    'affiliate list': [
      {'name': 'Limo Company 1', 'id': 1},
      {'name': 'Limo Company 2', 'id': 2},
      {'name': 'Limo Company 3', 'id': 3},
      {'name': 'Limo Company 4', 'id': 4},
    ],
    'white list': []
  };

  void addToWhitelist(company) {
    allUsers['affiliate list'].remove(company);
    allUsers['white list'].add(company);
    whitelistStreamController.sink.add(allUsers);
  }

  void removeFromWhitelist(company) {
    allUsers['white list'].remove(company);
    allUsers['affiliate list'].add(company);
    whitelistStreamController.sink.add(allUsers);
  }

  void dispose() {
    whitelistStreamController.close(); 
  }
}

final bloc = CompanyBloc();