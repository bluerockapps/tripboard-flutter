class Trip {
   String pickupLocation;
   String dropoffLocation;
   DateTime pickupDate;
   DateTime dropoffDate;
   double price;
   double deadHeadMilage;
   double numberPassangers;
   String vehicleType;
   String tripType;
   String numberOfBags;
   List typeofBags;

  Trip(this.pickupLocation, this.dropoffLocation, this.pickupDate, this.dropoffDate, this.price, this.deadHeadMilage, this.numberPassangers, this.vehicleType, this.tripType, this.numberOfBags, this.typeofBags);

}

