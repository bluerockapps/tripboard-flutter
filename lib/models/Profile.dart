class Profile {
  final int id;
  final String tenantId;
  final String companyLogo;
  final String companyName;
  final String firstName;
  final String lastName;
  final String email;
  final String addressStreet;
  final String addressCity;
  final String addressProvState;
  final String addressPostalZip;
  final DateTime dateCreated;
  final DateTime dateArchived;
  final DateTime dateLastAccess;
  final int userCreated;
  final int userArchive;
  final String handle;
  final bool subscription;

  Profile(this.id, this.tenantId, this.companyLogo, this.companyName, this.firstName, this.lastName, this.email, this.addressStreet, this.addressCity, this.addressPostalZip, this.addressProvState, this.dateArchived, this.dateCreated, this.dateLastAccess, this.userCreated, this.userArchive, this.handle, this.subscription);

  Profile.fromJson(Map<String, dynamic> json)
  : id = json['id'],
    tenantId = json['tenantId'],
    companyLogo = json['companyLogo'],
    companyName = json['companyName'],
    firstName = json['firstName'],
    lastName = json['lastName'],
    email = json['email'],
    addressStreet = json['addressStreet'],
    addressCity = json['addressCity'],
    addressProvState = json['addressProvState'],
    addressPostalZip = json['addressPostalZip'],
    dateCreated = json['dateCreated'],
    dateArchived = json['dateArchived'],
    dateLastAccess = json['dateLastAccess'],
    userCreated = json['userCreated'],
    userArchive = json['userArchive'],
    handle = json['handle'],
    subscription = json['subscription'];

  Map<String, dynamic> toJson() =>
  {
   'id': id,
   'tenantId': tenantId,
   'companylogo': companyLogo,
   'companyName': companyName,
   'firstName': firstName,
   'lastName': lastName,
   'email': email,
   'addressStreet': addressStreet,
   'addressCity': addressCity,
   'addressProvState': addressProvState,
   'addressPostalZip': addressPostalZip,
   'dateCreated': dateCreated,
   'dateArchived': dateArchived,
   'dateLastAccess': dateLastAccess,
   'userCreated': userCreated,
   'userArchive': userArchive,
   'handle': handle,
   'subscription': subscription
  };
}

