class User {
  final int id;
  final String tenantId;
  final String email;
  final String handle;
  final String role;
  final String token;

  User(this.id, this.tenantId, this.email, this.handle, this.role, this.token);

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        tenantId = json['tenantId'],
        email = json['email'],
        handle = json['handle'],
        role = json['role'],
        token = json['token'];

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'tenantId': tenantId,
      'email': email,
      'handle': handle,
      'role': role,
      'token': token
    };
}