import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:limo/views/home.dart';
import 'package:limo/auth/forgotpassword.dart';
import 'package:limo/service/auth.service.dart';
import 'package:limo/service/storage.service.dart';

class SignInView extends StatefulWidget {
  final String title = 'Sign In';

  @override
  State<StatefulWidget> createState() => SignInState();
}

class SignInState extends State<SignInView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  // final String logo = 'assets/parrotlogo.svg';
  final Widget svg = SvgPicture.asset(
    'assets/parrotlogo.svg',
  );
  dynamic user;
  String token;
  dynamic tripsAvailable;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('LimoParrot - Sign In', style: TextStyle(fontSize: 20)),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: SvgPicture.asset('assets/svg/parrot.svg', height: 250.0),
              ),
              TextFormField(
                controller: _emailController,
                decoration: const InputDecoration(
                    labelText: 'Email', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter email';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              TextFormField(
                controller: _passwordController,
                decoration: const InputDecoration(
                    labelText: 'Password', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 500,
                  height: 50.0,
                  buttonColor: Colors.blue, //  <-- dark color
                  textTheme: ButtonTextTheme.primary,
                  child: RaisedButton(
                    child:
                        const Text('Sign In', style: TextStyle(fontSize: 20)),
                    onPressed: () async {
                      print(_formKey.currentState.validate());
                      if (_formKey.currentState.validate()) {
                        _signin();
                      }
                    },
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 500,
                  height: 50.0,
                  buttonColor: Colors.blue, //  <-- dark color
                  textTheme: ButtonTextTheme.primary,
                  child: OutlineButton(
                    textColor: Colors.blue,
                    onPressed: () async {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgotPasswordView()),
                      );
                    },
                    child: Text('Forgot Password'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _signin() async {
    // var jsonData = '{ "companyName" : "Limo test", "email" : "test"  }';
    final _obj = (await signIn(
      _emailController.text,
      _passwordController.text,
    ));
    setState(() {
      StorageUtil.putString("token", _obj["token"]);
      StorageUtil.putString("email", _obj["user.email"]);
      StorageUtil.putString("firstName", _obj["user.first_name"]);
      StorageUtil.putString("lastName", _obj["user.last_name"]);
      StorageUtil.putString("tenantId", _obj["user.tenant_id"]);
      // StorageUtil.putString("profile", jsonData);
      // Signin will send Profile Data back as well

    });
    if (_obj["token"] != null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeView()),
      );
    }
  }
}
