import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:limo/views/home.dart';
import 'package:limo/auth/signin.dart';
import 'package:limo/service/auth.service.dart';
import 'package:limo/service/storage.service.dart';
import 'package:limo/views/profile/registerprofileinfo.dart';

// import 'package:limo/models/User.dart';

class RegisterView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterState();
}

class RegisterState extends State<RegisterView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _success;
  String _email;
  String _token;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Register with LimoParrot', style: TextStyle(fontSize: 20)),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: SvgPicture.asset('assets/svg/parrot.svg', height: 250.0),
              ),
              TextFormField(
                controller: _emailController,
                decoration: const InputDecoration(
                    labelText: 'Email', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter email';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              TextFormField(
                controller: _passwordController,
                decoration: const InputDecoration(
                    labelText: 'Password', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter some text';
                  }
                  return null;
                },
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 500,
                  height: 50.0,
                  buttonColor: Colors.blue, //  <-- dark color
                  textTheme: ButtonTextTheme.primary,
                  child: RaisedButton(
                    child:
                        const Text('Register', style: TextStyle(fontSize: 20)),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _register();
                      }
                    },
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(_success == null
                    ? ''
                    : (_success
                        ? 'Successfully registered ' + _email
                        : 'Registration failed')),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _register() async {
    final _obj = (await register(
      _emailController.text,
      _passwordController.text,
    ));
    setState(() {
      StorageUtil.putString("token", _obj["token"]);
      StorageUtil.putString("email", _obj["user.email"]);
      StorageUtil.putString("firstName", _obj["user.first_name"]);
      StorageUtil.putString("lastName", _obj["user.last_name"]);
      StorageUtil.putString("tenantId", _obj["user.tenant_id"]);
    });
    if (_obj["token"] == null) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SignInView()),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RegisterProfileInfo()),
      );
    }
  }
}
