import 'package:flutter/material.dart';
import './register.dart';
import './signin.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:limo/views/home.dart';

class AuthView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20.0),
              padding: const EdgeInsets.symmetric(vertical: 25.0),
              alignment: Alignment.center,
              child: Column(
                children: <Widget>[
                  SvgPicture.asset('assets/svg/bigparrot.svg', height: 250.0),
                  SizedBox(
                    height: 35.0,
                  ),
                  Text('LimoParrot',
                      style: TextStyle(color: Colors.black, fontSize: 40)),
                  SizedBox(
                    height: 35.0,
                  ),
                  ButtonTheme(
                    minWidth: 500,
                    height: 50.0,
                    buttonColor: Colors.blue, //  <-- dark color
                    textTheme: ButtonTextTheme.primary,
                    child: RaisedButton(
                      child:
                          const Text('Sign In', style: TextStyle(fontSize: 20)),
                      onPressed: () => _pushPage(context, SignInView()),
                      //  onPressed: () => _pushPage(context, HomeView()),
                      // MaterialPageRoute(builder: (context) => HomeView()),
                    ),
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  ButtonTheme(
                    minWidth: 500,
                    height: 50.0,
                    buttonColor: Colors.blue, //  <-- dark color
                    textTheme: ButtonTextTheme.primary,
                    child: RaisedButton(
                      child: const Text('Register with Us',
                          style: TextStyle(fontSize: 20)),
                      onPressed: () => _pushPage(context, RegisterView()),
                    ),
                  ),
                  // RaisedButton(
                  //   child: const Text('Sign In'),
                  //   onPressed: () => _pushPage(context, SignInView()),
                  // ),
                  // RaisedButton(
                  //   child: const Text('Register'),
                  //   onPressed: () => _pushPage(context, RegisterView()),
                  // ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _pushPage(BuildContext context, Widget page) {
    Navigator.of(context).push(
      MaterialPageRoute<void>(builder: (_) => page),
    );
  }
}
