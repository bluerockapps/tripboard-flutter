import 'package:flutter/material.dart';

import 'signin.dart';
import 'package:limo/service/auth.service.dart';

class ForgotPasswordView extends StatefulWidget {
  final String title = 'Forgot Password';
  @override
  State<StatefulWidget> createState() => ForgotPasswordState();
}

class ForgotPasswordState extends State<ForgotPasswordView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController = TextEditingController();
  bool _success;
  String _email;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            TextFormField(
              controller: _passwordController,
              decoration: const InputDecoration(labelText: 'Password'),
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Please enter a password';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _passwordConfirmController,
              decoration: const InputDecoration(labelText: 'Confirm Passwordd'),
              validator: (String value) {
                if (value.isEmpty) {
                  return 'Please enter password confirmation';
                }
                return null;
              },
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              alignment: Alignment.center,
              child: RaisedButton(
                onPressed: () async {
                  if (_formKey.currentState.validate()) {
                    _resetPassword();
                  }
                },
                child: const Text('Reset Password'),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: Text(_success == null
                  ? ''
                  : (_success
                      ? 'Password has been reset '
                      : 'Password reset failed')),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _passwordController.dispose();
    _passwordConfirmController.dispose();
    super.dispose();
  }

  void _resetPassword() async {
    // print(_emailController.text);
    await sendPasswordEmail(
      _passwordController.text
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignInView()),
    );
  }
}
