import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:limo/auth/signin.dart';
import 'package:limo/service/auth.service.dart';

class ForgotPasswordView extends StatefulWidget {
  final String title = 'Forgot Password';
  @override
  State<StatefulWidget> createState() => ForgotPasswordState();
}

class ForgotPasswordState extends State<ForgotPasswordView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  bool _success;
  String _email;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('LimoParrot Forgot Password', style: TextStyle(fontSize: 20)),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: SvgPicture.asset('assets/svg/parrot.svg', height: 250.0),
              ),
              TextFormField(
                controller: _emailController,
                decoration: const InputDecoration(
                    labelText: 'Email', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter email';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 500,
                  height: 50.0,
                  buttonColor: Colors.blue, //  <-- dark color
                  textTheme: ButtonTextTheme.primary,
                  child: RaisedButton(
                    child: const Text('Send Forgot Password Email',
                        style: TextStyle(fontSize: 20)),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _forgotPassword();
                      }
                    },
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(_success == null
                    ? ''
                    : (_success
                        ? 'Password reset link sent to ' + _email
                        : 'Password reset failed')),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _forgotPassword() async {
    // print(_emailController.text);
    await sendPasswordEmail(_emailController.text);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignInView()),
    );
  }
}
