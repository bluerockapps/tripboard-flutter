import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:limo/auth/forgotpassword.dart';
import 'package:limo/service/auth.service.dart';

class ResetPasswordView extends StatefulWidget {
  final String title = 'Sign In';

  @override
  State<StatefulWidget> createState() => ResetPasswordState();
}

class ResetPasswordState extends State<ResetPasswordView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController = TextEditingController();
  final Widget svg = SvgPicture.asset(
    'assets/parrotlogo.svg',
  );
  bool _success;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('LimoParrot - Reset Password', style: TextStyle(fontSize: 20)),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: SvgPicture.asset('assets/svg/parrot.svg', height: 250.0),
              ),
              TextFormField(
                controller: _passwordController,
                decoration: const InputDecoration(
                    labelText: 'Password', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter password';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              TextFormField(
                controller: _passwordController,
                decoration: const InputDecoration(
                    labelText: 'Confirm Password', border: OutlineInputBorder()),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter conformation password';
                  }
                  return null;
                },
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                alignment: Alignment.center,
                child: ButtonTheme(
                  minWidth: 500,
                  height: 50.0,
                  buttonColor: Colors.blue, //  <-- dark color
                  textTheme: ButtonTextTheme.primary,
                  child: RaisedButton(
                    child:
                        const Text('Reset Password', style: TextStyle(fontSize: 20)),
                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        _resetPassword();
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _passwordController.dispose();
    _passwordConfirmController.dispose();
    super.dispose();
  }

  void _resetPassword() async {
    await signIn(
      _passwordController.text,
      _passwordConfirmController.text,
    );
  }
}
