import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;



final _http = 'http://10.0.2.2:9001/';


// final tripsList = [
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '3', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020","01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '5', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '2', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '4', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '7', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '5', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '3', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '4', ['golf bag','carry on','suite case']},
//   {"Toronto", "Pearson Airport", "01/09/2020", "01/09/2020", 200, 20, 6,
//       'Van', 'Wedding', '3', ['golf bag','carry on','suite case']},
// ];

Future availableTrips(tenantId) async {
  final http.Response response = await http.get(
    _http+'trips/available/id='+tenantId,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );
  if (response.statusCode == 201) {
    final body = json.decode(response.body);
    return body;
    
  } else {
    throw Exception('Failed to get available trips.');
  }
}

Future myTrips(tenantId) async {
  final http.Response response = await http.get(
    _http+'trips/my/id='+tenantId,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  );
  if (response.statusCode == 201) {
    final body = json.decode(response.body);
    return body;
  } else {
    throw Exception('Failed to get available trips.');
  }
}
