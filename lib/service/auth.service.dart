import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;


final _http = 'http://10.0.2.2:9001/';

Future register(email, password) async {
  final http.Response response = await http.post(
    _http+'auth/signup/email',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({'email': email, 'password': password}),
  );
  if (response.statusCode == 201) {
    Map<String, dynamic> userJson = json.decode(response.body);
    return userJson;
  } else {
    throw Exception('Failed to sign up user.');
  }
}

Future signIn(email, password) async {
  final http.Response response = await http.post(
    _http+'auth/login',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({'email': email, 'password': password}),
  );
  if (response.statusCode == 201) {
    final body = json.decode(response.body);
    return body;
  } else {
    throw Exception('Failed to sign in user.');
  }
}

Future sendPasswordEmail(email) async {
  final http.Response response = await http.post(
    _http+'auth/login',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({'email': email}),
  );
  if (response.statusCode == 201) {
     Map<String, dynamic> userJson = json.decode(response.body);
    return userJson;
  } else {
    throw Exception('Failed to sign in user.');
  }
}

Future resetPassword(email) async {
  final http.Response response = await http.post(
    _http+'auth/login',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode({'email': email}),
  );
  if (response.statusCode == 201) {
     Map<String, dynamic> userJson = json.decode(response.body);
    return userJson;
  } else {
    throw Exception('Failed to sign in user.');
  }
}