import 'package:flutter/material.dart';

Widget buildSearch(BuildContext context) {
  return Container(
    child: Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(10.0,0.0,10.0,0.0),
          child: TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.search),
            ),
          ),
        ),
      ],
    ),
  );
}
