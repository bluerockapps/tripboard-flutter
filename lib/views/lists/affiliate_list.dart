import 'package:flutter/material.dart';
import 'package:limo/bloc/companies_bloc.dart';

class AffiliateList extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      body: AffiliateListWidget(),
    );
  }
}

class AffiliateListWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: bloc.allUsers,
      stream: bloc.getStream,
      builder: (context, snapshot) {
        return snapshot.data["affiliate list"].length > 0
            ? affiliateListBuilder(snapshot)
            : Center(child: Text("All companies have been added"));
      },
    );
  }
}

Widget affiliateListBuilder(snapshot) {
  return ListView.builder(
    itemCount: snapshot.data["affiliate list"].length,
    itemBuilder: (BuildContext context, i) {
      final affiliateList = snapshot.data["affiliate list"];
      return ListTile(
        title: Text(affiliateList[i]['name']),
        trailing: IconButton(
          icon: Icon(Icons.add_circle),
          color: Colors.green,
          onPressed: () {
            bloc.addToWhitelist(affiliateList[i]);
          },
        ),
        onTap: () {},
      );
    },
  );
}

