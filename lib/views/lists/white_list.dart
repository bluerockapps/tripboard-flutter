import 'package:flutter/material.dart';
import 'package:limo/bloc/companies_bloc.dart';

class WhiteList extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: bloc.getStream,
        initialData: bloc.allUsers,
        builder: (context, snapshot) {
          return snapshot.data['white list'].length > 0
              ? Column(
                  children: <Widget>[
                    Expanded(child: whiteListBuilder(snapshot)),
                    RaisedButton(
                      onPressed: () {},
                      child: Text("save"),
                      textColor: Colors.white,
                      color: Colors.green,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(20.0)),
                    ),
                    SizedBox(height: 40)
                  ],
                )
              : Center(child: Text("You haven't selected any companies yet"));
        },
      ),
    );
  }
}

Widget whiteListBuilder(snapshot) {
  return ListView.builder(
    itemCount: snapshot.data["white list"].length,
    itemBuilder: (BuildContext context, i) {
      final whiteList = snapshot.data["white list"];
      return ListTile(
        title: Text(whiteList[i]['name']),
        trailing: IconButton(
          icon: Icon(Icons.remove_circle),
          color: Colors.redAccent,
          onPressed: () {
            bloc.removeFromWhitelist(whiteList[i]);
          },
        ),
        onTap: () {},
      );
    },
  );
}
