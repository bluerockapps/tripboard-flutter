import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:limo/models/Trip.dart';
import 'package:limo/views/newtrip/templates.dart';
import '../demand.dart';
import '../home.dart';
import 'confirmtrip.dart';

class NewTripView extends StatelessWidget {
  final Trip trip;
  NewTripView({Key key, @required this.trip}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController _titleController = new TextEditingController();
    _titleController.text = trip.pickupLocation;
    TextEditingController _desinationController = new TextEditingController();
    _desinationController.text = trip.dropoffLocation;
    TextEditingController _commentController = new TextEditingController();
    _commentController.text = trip.dropoffLocation;
    TextEditingController _passengerController = new TextEditingController();
    _passengerController.text = trip.dropoffLocation;
    // update model to include

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Create Trip'),
        leading: GestureDetector(
         onTap: () {
           Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HomeView()),
          );
         },
         child: Icon(
           Icons.arrow_back,
         ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SvgPicture.asset('assets/svg/parrot.svg', height: 25.0),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.blue)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => TemplateView()),
                      );
                    },
                    color: Colors.blue,
                    textColor: Colors.white,
                    child: const Text('Templates',
                        style: TextStyle(fontSize: 20))),
              ),
              SizedBox(
                height: 25.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Pickup Date & Time'),
              ),
              Container(
                height: 100,
                child: CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.dateAndTime,
                  initialDateTime: DateTime(2020, 5, 20, 11, 33),
                  onDateTimeChanged: (DateTime newDateTime) {
                    //Do Some thing
                  },
                  use24hFormat: false,
                  minuteInterval: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _titleController,
                  decoration: const InputDecoration(
                      labelText: 'Pickup Location',
                      border: OutlineInputBorder()),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter a Pickup Location';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _desinationController,
                  decoration: const InputDecoration(
                      labelText: 'Dropoff Location',
                      border: OutlineInputBorder()),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Please enter a Destination';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Dropoff Date & Time'),
              ),
              Container(
                height: 100,
                child: CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.dateAndTime,
                  initialDateTime: DateTime(2020, 5, 20, 11, 33),
                  onDateTimeChanged: (DateTime newDateTime) {
                    //Do Some thing
                  },
                  use24hFormat: false,
                  minuteInterval: 1,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)),
                      textColor: Colors.white,
                      color: Colors.orange,
                      child: Text("Cancel"),
                      onPressed: () {
                        trip.pickupDate = DateTime.now();
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => DemandView()),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.lightBlue)),
                      textColor: Colors.white,
                      color: Colors.lightBlue,
                      child: Text("Next"),
                      onPressed: () {
                        trip.pickupDate = DateTime.now();
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  NewTripConfirmView(trip: trip)),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
