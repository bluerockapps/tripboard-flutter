import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:limo/models/Trip.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class NewTripConfirmView extends StatefulWidget {
  final Trip trip;
  NewTripConfirmView({Key key, @required this.trip}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return NewTripConfirmViewState();
  }
}

class NewTripConfirmViewState extends State<NewTripConfirmView>
    with TickerProviderStateMixin {
  AnimationController _animationController;

  double _containerPaddingLeft = 20.0;
  double _animationValue;
  double _translateX = 0;
  double _translateY = 0;
  double _rotate = 0;
  double _scale = 1;

  bool rememberMe = false;
  bool show;
  bool sent = false;
  Color _color = Colors.lightBlue;

  void _onRememberMeChanged(bool newValue) => setState(() {
        rememberMe = newValue;

        if (rememberMe) {
          //add functionality for templates
        } else {}
      });
  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1300));
    show = true;
    _animationController.addListener(() {
      setState(() {
        show = false;
        _animationValue = _animationController.value;
        if (_animationValue >= 0.2 && _animationValue < 0.4) {
          _containerPaddingLeft = 100.0;
          _color = Colors.green;
        } else if (_animationValue >= 0.4 && _animationValue <= 0.5) {
          _translateX = 80.0;
          _rotate = -20.0;
          _scale = 0.1;
        } else if (_animationValue >= 0.5 && _animationValue <= 0.8) {
          _translateY = -20.0;
        } else if (_animationValue >= 0.81) {
          _containerPaddingLeft = 20.0;
          sent = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController _commentController = new TextEditingController();
    _commentController.text = ""; //trip.dropoffLocation;
    TextEditingController _passengerController = new TextEditingController();
    _passengerController.text = ""; //trip.dropoffLocation;

    // update model to include

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('Create Trip'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SvgPicture.asset('assets/svg/parrot.svg', height: 25.0),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                    decoration: InputDecoration(
                        hintText: "Number of Passengers",
                        border: OutlineInputBorder()),
                    keyboardType: TextInputType.number),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MultiSelectFormField(
                  autovalidate: false,
                  titleText: 'Vehicle Type',
                  dataSource: [
                    {
                      "display": "Town Car",
                      "value": "Town Car",
                    },
                    {
                      "display": "Limo",
                      "value": "Limo",
                    },
                    {
                      "display": "Van",
                      "value": "Van",
                    },
                    {
                      "display": "Car",
                      "value": "Car",
                    },
                    {
                      "display": "Bus",
                      "value": "Bus",
                    },
                    {
                      "display": "Any",
                      "value": "Any",
                    },
                  ],
                  textField: 'display',
                  valueField: 'value',
                  okButtonLabel: 'OK',
                  cancelButtonLabel: 'CANCEL',
                  // required: true,
                  hintText: 'Please choose one or more',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                    decoration: InputDecoration(
                        hintText: "Amount of Luggage",
                        border: OutlineInputBorder()),
                    keyboardType: TextInputType.number),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MultiSelectFormField(
                  autovalidate: false,
                  titleText: 'Luggage Type',
                  dataSource: [
                    {
                      "display": "Suitcase",
                      "value": "Suitcase",
                    },
                    {
                      "display": "Golf Clubs",
                      "value": "Golf Clubs",
                    },
                    {
                      "display": "Carry On",
                      "value": "Carry On",
                    },
                    {
                      "display": "Duffel Bag",
                      "value": "Duffel Bag",
                    },
                    {
                      "display": "Travel Totes",
                      "value": "Travel Totes",
                    },
                    {
                      "display": "Garment Bags",
                      "value": "Garment Bags",
                    },
                  ],
                  textField: 'display',
                  valueField: 'value',
                  okButtonLabel: 'OK',
                  cancelButtonLabel: 'CANCEL',
                  // required: true,
                  hintText: 'Please choose one or more',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: MultiSelectFormField(
                  autovalidate: false,
                  titleText: 'Affiliate Groups',
                  dataSource: [
                    {
                      "display": "YYZ Sedan 1",
                      "value": "YYZ Sedan 1",
                    },
                    {
                      "display": "YYZ Sedan 2",
                      "value": "YYZ Sedan 2",
                    },
                    {
                      "display": "YYZ SUV",
                      "value": "YYZ SUV",
                    },
                    {
                      "display": "Any",
                      "display": "Any",
                    },
                  ],
                  textField: 'display',
                  valueField: 'value',
                  okButtonLabel: 'OK',
                  cancelButtonLabel: 'CANCEL',
                  // required: true,
                  hintText: 'Please choose one or more',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _commentController,
                  decoration: const InputDecoration(
                      labelText: 'Additional Comments',
                      border: OutlineInputBorder()),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Additional Comments';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(25.0),
                  child: Row(
                      children: <Widget>[
                                GestureDetector(
                          onTap: () {
                            _animationController.forward();
                          },
                          child: AnimatedContainer(
                                decoration: BoxDecoration(
                                  color: _color,
                                  borderRadius: BorderRadius.circular(100.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: _color,
                                      blurRadius: 21,
                                      spreadRadius: -15,
                                      offset: Offset(
                                        0.0,
                                        20.0,
                                      ),
                                    )
                                  ],
                                ),
                                padding: EdgeInsets.only(
                                    left: _containerPaddingLeft,
                                    right: 20.0,
                                    top: 10.0,
                                    bottom: 10.0),
                                duration: Duration(milliseconds: 400),
                                curve: Curves.easeOutCubic,
                                child: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    (!sent)
                                        ? AnimatedContainer(
                                            duration: Duration(milliseconds: 400),
                                            child: Icon(Icons.send),
                                            curve: Curves.fastOutSlowIn,
                                            transform: Matrix4.translationValues(
                                                _translateX, _translateY, 0)
                                              ..rotateZ(_rotate)
                                              ..scale(_scale),
                                          )
                                        : Container(),
                                    AnimatedSize(
                                      vsync: this,
                                      duration: Duration(milliseconds: 600),
                                      child: show
                                          ? SizedBox(width: 10.0)
                                          : Container(),
                                    ),
                                    AnimatedSize(
                                      vsync: this,
                                      duration: Duration(milliseconds: 200),
                                      child: show
                                          ? Text("Confirm Trip")
                                          : Container(),
                                    ),
                                    AnimatedSize(
                                      vsync: this,
                                      duration: Duration(milliseconds: 200),
                                      child:
                                          sent ? Icon(Icons.done) : Container(),
                                    ),
                                    AnimatedSize(
                                      vsync: this,
                                      alignment: Alignment.topLeft,
                                      duration: Duration(milliseconds: 600),
                                      child: sent
                                          ? SizedBox(width: 10.0)
                                          : Container(),
                                    ),
                                    AnimatedSize(
                                      vsync: this,
                                      duration: Duration(milliseconds: 200),
                                      child: sent ? Text("Done") : Container(),
                                    ),
                                  ],
                                ))),
                                Checkbox(value: rememberMe, onChanged: _onRememberMeChanged,),
                                Text("Save As Template")
                              ])),
            ],
          ),
        ),
      ),
    );
  }
}
