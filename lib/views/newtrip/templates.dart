import 'package:flutter/material.dart';

class TemplateView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TemplateViewState();
  }
}

class TemplateViewState extends State<TemplateView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Templates'),
      ),
      body: new GridView.count(
        crossAxisCount: 2,
        children: new List<Widget>.generate(4, (index) {
          return new GridTile(
            child: new Card(
                color: Colors.blue.shade200,
                child: new Center(
                  child: new Text('trip $index'),
                )),
          );
        }),
      ),
    );
  }
}
