import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';

class RegisterProfileInfo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => RegisterProfileInfoState();
}

class RegisterProfileInfoState extends State<RegisterProfileInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 40.0,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.account_circle),
                  labelText: 'Company Name',
                ),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter a Company Name';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.email),
                  labelText: 'Email',
                ),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter a Email';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              TextFormField(
                decoration: const InputDecoration(
                  icon: Icon(Icons.map),
                  labelText: 'Service Area',
                ),
                validator: (String value) {
                  if (value.isEmpty) {
                    return 'Please enter a Area';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 25.0,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: MultiSelectFormField(
                      autovalidate: false,
                      titleText: 'Fleet Vehicle Types',
                      dataSource: [
                        {
                          "display": "Town Car",
                          "value": "Town Car",
                        },
                        {
                          "display": "Limo",
                          "value": "Limo",
                        },
                        {
                          "display": "Van",
                          "value": "Van",
                        },
                        {
                          "display": "Car",
                          "value": "Car",
                        },
                        {
                          "display": "Bus",
                          "value": "Bus",
                        },
                        {
                          "display": "Any",
                          "value": "Any",
                        },
                      ],
                      textField: 'display',
                      valueField: 'value',
                      okButtonLabel: 'OK',
                      cancelButtonLabel: 'CANCEL',
                      // required: true,
                      hintText: 'Please choose one or more',
                    ),
                  ),
                ],
              ),
              Spacer(),
              RaisedButton(
                child: Text("Create Profile"),
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}