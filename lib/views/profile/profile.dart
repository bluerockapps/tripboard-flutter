import 'package:flutter/material.dart';
import 'package:limo/views/lists/affiliate_list.dart';
import 'package:limo/views/lists/white_list.dart';
import 'package:limo/views/profile/profileinfo.dart';

import '../home.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            leading: GestureDetector(
         onTap: () {
           Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => HomeView()),
          );
         },
         child: Icon(
           Icons.arrow_back,
           color: Colors.blue,
         ),
        ),
            backgroundColor: Colors.white,
            title: new Text("Account"),
            elevation: 4.0,
            centerTitle: true,
            textTheme: TextTheme(title: TextStyle(color: Colors.black, fontSize: 20)),
            bottom: TabBar(
              labelColor: Colors.blue,
              indicatorColor: Colors.blue,
              tabs: [
                Tab(
                  text: 'Account Info',
                ),
                Tab(
                  text: 'Affiliate List',
                ),
                Tab(
                  text: 'White List',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Container(child: ProfileInfo(profile: null,)),
              Container(child: AffiliateList()),
              Container(child: WhiteList())
            ],
          ),
        ),
      ),
    );
  }
}
