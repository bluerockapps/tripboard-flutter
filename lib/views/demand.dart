import 'package:flutter/material.dart';
import 'tripcard.dart';
import 'package:limo/widgets/calendar.dart';
import 'package:limo/widgets/search.dart';


import 'package:limo/models/Trip.dart';

final List<Trip> tripsList = [
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '3', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '5', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '2', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '4', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '7', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '5', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '3', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '4', ['golf bag','carry on','suite case']),
  Trip("Toronto", "Pearson Airport", DateTime.now(), DateTime.now(), 200, 20, 6,
      'Van', 'Wedding', '3', ['golf bag','carry on','suite case']),
];

class DemandView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => DemandViewState();
}

class DemandViewState extends State<DemandView> {
  
  @override
  void initState() {
    // final _tenantId = StorageUtil.getString("tenant_id");
    // final List<Trip> _tripsList = availableTrips(_tenantId);
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    var newTrip =
        new Trip(null, null, null, null, null, null, null, null, null, null, null);
    return Scaffold(
          body: Column(
            children: <Widget>[
              SizedBox(
                height: 25.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Text('Available Trips',
                    style: TextStyle(color: Colors.black, fontSize: 20)),
              ),
              Container(child: Calendar()),
              Container(child: buildSearch(context)),
              Expanded(
                flex: 4,
                child: new ListView.builder(
                    // itemCount: tripsList.length,
                    itemBuilder: (BuildContext context, int index) =>
                        buildTripCard(context, index, tripsList)),
              ),
            ],
          ),
    );
  }
}
