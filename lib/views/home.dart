import 'package:flutter/material.dart';
import 'package:limo/auth/auth.dart';
import 'package:limo/views/profile/profile.dart';
import 'package:limo/models/Trip.dart';
import 'package:limo/views/newtrip/createtrip.dart';
import 'demand.dart';
import 'trip.dart';

class HomeView extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomeViewState();
  }
}

class HomeViewState extends State<HomeView> {
  bool _showNavBar = true;
  int _currentIndex = 0;
  final List<Widget> _children = [
    DemandView(),
    NewTripView(trip: null,),
    TripView(tenantId:'sddsfsdf'),
  ];
  int index = 0;
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
            icon: Icon( 
              Icons.account_circle,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProfileView()),
          );
            },
          ),
          IconButton(
            icon: Icon( 
              Icons.exit_to_app,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AuthView()),
          );
            },
          )
        ],
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: _showNavBar
          ? BottomNavigationBar(
            backgroundColor: Colors.white,
            selectedItemColor: Colors.black,
            unselectedItemColor: Colors.blue,
            type: BottomNavigationBarType.fixed,
              onTap: onTabTapped,
              currentIndex: _currentIndex,
              items: [
                BottomNavigationBarItem(
                    icon: new Icon(Icons.time_to_leave),
                    title: new Text("Available Trips")),
                BottomNavigationBarItem(
                    icon: new Icon(Icons.add_circle_outline),
                    title: new Text("Create Trip")),
                BottomNavigationBarItem(
                    icon: new Icon(Icons.departure_board),
                    title: new Text("My Trips")),
              ],
            )
          : Container(
              color: Colors.white,
              width: MediaQuery.of(context).size.width,
            ),
    );
  }

  void onTabTapped(int index) {
    var newTrip =
        new Trip(null, null, null, null, null, null, null, null, null, null, null);
    if (index == 1) {
      _showNavBar = false;
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NewTripView(trip: newTrip,))
      );
    }
    setState(() {
      _currentIndex = index;
    });
  }
}