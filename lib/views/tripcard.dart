import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:limo/widgets/timeline.dart';

Widget buildTripCard(BuildContext context, int index, tripsList) {
  final trip = tripsList[index];
  return new Container(
    child: Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 5.0, bottom: 0.0),
              child: Row(
                children: <Widget>[
                  Text(
                    "\$${trip.price.toStringAsFixed(0)}",
                    style: new TextStyle(fontSize: 26.0),
                  ),
                  Spacer(),
                  Text(trip.vehicleType, style: new TextStyle(fontSize: 16.0)),
                ],
              ),
            ),
            Container(
              // padding: const EdgeInsets.only(top:00.0, bottom: 0.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      trip.tripType,
                      style: new TextStyle(fontSize: 20.0),
                    ),
                  ]),
            ),
            Container(
              padding: const EdgeInsets.only(left: 0.0, bottom: 2.0),
              child: Row(children: <Widget>[
                Text(
                  trip.deadHeadMilage.toStringAsFixed(0) + ' km',
                  style: new TextStyle(fontSize: 16.0),
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: Text(
                      'Deadhead',
                      style: new TextStyle(fontSize: 16.0),
                    )),
                Spacer(),
              ]),
            ),
            Column(children: <Widget>[
              Container(
                alignment: Alignment.bottomRight,
                child: Text(trip.numberOfBags + ' luggage',
                    style: new TextStyle(fontSize: 16.0)),
              ),
              Container(
                alignment: Alignment.bottomRight,
                child: Text(
                    trip.numberPassangers.toStringAsFixed(0) + ' passengers',
                    style: new TextStyle(fontSize: 16.0)),
              ),
            ]),
            Container(
              decoration: BoxDecoration(color: Colors.white),
              margin: EdgeInsets.only(
                bottom: 0,
              ),
              padding: EdgeInsets.only(
                top: 10,
                // left: 10,
                // bottom: 10,
              ),
              child: Column(
                children: <Widget>[
                  timelineRow(
                      trip.pickupLocation,
                      DateFormat('HH:mm:ss')
                          .format(trip.pickupDate)
                          .toString()),
                  timelineLastRow(
                      trip.dropoffLocation,
                      DateFormat('HH:mm:ss')
                          .format(trip.dropoffDate)
                          .toString()),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  const SizedBox(height: 30),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.blue)),
                    onPressed: () {},
                    color: Colors.blue,
                    textColor: Colors.white,
                    child:
                        const Text('Place Bid', style: TextStyle(fontSize: 20)),
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.red)),
                    onPressed: () {},
                    color: Colors.red,
                    textColor: Colors.white,
                    child: const Text('Cancel Trip',
                        style: TextStyle(fontSize: 20)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
// }
